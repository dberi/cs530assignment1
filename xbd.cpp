#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cstring>
#include <vector>

using namespace std;

int main(int argc, char* argv[]){

    bool isOutBinary = false;       //Hex format currently
    bool isFileBinary = true;       //Binary format currently
    int fileArgIndex = 2;           //Binary format currently

    //First check for args
    if(strcmp(argv[1], "-b") == 0)
        isOutBinary = true;         //Change format to binary
    else
        fileArgIndex = 1;           //Change format to hex

    //Second create ifstream
    string fileName(argv[fileArgIndex]);
    ifstream infile(fileName);

    //Third put ifstream into sstream
    if(infile){
        stringstream ss;
        ss << infile.rdbuf();
        string ssLine;              //Each line in file
        string fullText = "";            //Every character in file

        //Store the file line by line in a vector
        vector<string> buffer;
        while(getline(ss, ssLine))
            buffer.push_back(ssLine);

        //Fourth determine if the file is ascii or binary
        for(int n = 0; n < buffer.size(); n++){
            ssLine = buffer[n];
            for(int i = 0; i < ssLine.length(); i++){
                //If this char is not a 1 or 0 then set file as ascii and break
                //If file is already confirmed binary break
                if(isFileBinary && (ssLine[i] == '1' || ssLine[i] == '0' || ssLine[i] == ' '))
                    continue;
                isFileBinary = false;
                break;
            }
            fullText.append(ssLine);        //Storing every character in file as a string
        }

        //Fifth read file and convert

        /* Algorithm's output: 3(or 2) strings named:
          1. asciiText
          2. hexText
          3. binaryText
        */

        //~ IF BINARY:
          // Make a copy of fullText called binaryText.
          // For loop for taking 8 char chunk after checking if the string has 8 chars avail.
          // 1 Put 8 char chunk in 'binary_chunk' and make a copy of 'binary_chunk' named 'binary_string'.
          // 1.b If isOutBinary = True, put 8 char chunk in 'binary_chunk'.
          // 2. Convert that 'binary_chunk' into 'hex_string' and 'binary_string'into 'Ascii_char'. If not convertable to Ascii, put the character '.'.
          // 2.b Convert 'binary chunk' to 'Ascii_char'. If not convertable to Ascii, put the character '.'.
          // 3. Put 'hex_string' in hexText and 'Ascii_char' in asciiText.
          // 3.b We already have a copy called binaryText. Put 'Ascii_char' in asciiText.

        //~IF ASCII:
          // Make a copy of fullText called asciiText.
          // For loop taking 2 char chunk after checking if the string has 2 chars avail.
          //1. Put 2 char chunk in 'ascii_chunk'
          //1.b If isOutBinary = True, Put 2 char chunk in 'ascii_chunk'.
          //2. Convert 'ascii_chunk' to 'hex_string'.
          //2.b Convert 'ascii_chunk' to 'binary_string'.
          //3. Put 'hex_string' to hexText. We already have asciiText.
          //3.b Put 'binary_string' to binaryText. We already have asciiText.

        //Sixth output result, maybe durring fifth step
        cout << fullText << endl;         //Testing SS

        infile.close();
    }
}
